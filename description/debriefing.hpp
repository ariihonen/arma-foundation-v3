//Mission ending states, used via BIS_fnc_endMission from end.sqf
class CfgDebriefing
{
	class Started
	{
		title = "Mission Failed";
		subtitle = "You didn't even try??";
		description = "Scrub";
		picture = "\A3\ui_f\data\map\mapcontrol\taskIconFailed_ca.paa";
		pictureColor[] = {1.0,0.3,0.2,1.0};
	};

	class Win
	{
		title = "Mission Successful";
		subtitle = "Yay you";
		picture = "\A3\ui_f\data\map\mapcontrol\taskIconDone_ca.paa";
		picturecolor[] = {0.0,0.3,0.6,1};
	};

	class Dead
	{
		title = "You are dead";
		subtitle = "Mission over";
		picture = "KIA";
		pictureColor[] = {0.6,0.1,0.2,1};
	};
};
