/*
Checks whether marker position is legal

Syntax:
    [marker, position] call ARTR_fnc_mm_checkMarkerPos

Params:
    0 - marker: String - name of marker to check
    1 - position: Position array - position to check marker rules against

Return value:
    BOOL - whether marker can be placed in the position or not
*/
params ["_marker", "_position"];

_markerRules = ((missionNamespace getVariable "ARTR_movableMarkers") get _marker);
_markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
_markerPlacement params [
    ["_includeAreas", nil, [[]]],
    ["_excludeAreas", [], [[]]],
    ["_waterMode", 0, [0]],
    ["_checkEmpty", true, [true]],
    ["_vehicleType", "B_Soldier_F", [""]]
];

_posLegal = true;

if !(isNil "_includeAreas") then
{
    _found = _includeAreas findIf { _position inArea _x; };
    if (_found < 0) then { _posLegal = false; };
};

if (_posLegal) then
{
    _found = _excludeAreas findIf { _position inArea _x; };
    if (_found > -1) then { _posLegal = false; };

    if (_posLegal && _waterMode != 1) then
    {
        if ( (_waterMode == 0 && !(surfaceIsWater _position)) || (_waterMode == 2) && (surfaceIsWater _position) ) then { _posLegal = true; } else { _posLegal = false; };
    };
};

_posLegal
