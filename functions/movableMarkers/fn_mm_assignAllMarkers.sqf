/*
Assigns correct owners for all markers.

Syntax:
    [] call ARTR_fnc_mm_assignAllMarkers

Params:
    N/A

Return value:
    true
*/
_allMarkers = keys (missionnamespace getVariable "ARTR_movableMarkers");

{
    [_x] call ARTR_fnc_mm_markerOwner;
} forEach _allMarkers;

true
