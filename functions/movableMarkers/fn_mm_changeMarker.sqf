/*
Changes marker based on new position/rotation/text

Syntax:
    [marker, position, rotation, text] call ARTR_fnc_mm_changeMarker

Params:
    0 - marker: String - name of marker to be altered
    1 - position: Position array - position to move the marker to
    2 - rotation: Number - rotation to turn the marker to
    3 - text: String - new marker text to add

Return value:
    true
*/


params [
    ["_marker", "", [""]],
    ["_position", nil, [[]]],
    ["_rotation", nil, [0]],
    ["_text", nil, [""]]
];

if (isNil "_position") then { _position = markerPos _marker; };
if (isNil "_rotation") then { _rotation = markerDir _marker; };
if (isNil "_text") then { _text = markerText _marker; };

_markerRules = (missionNamespace getVariable "ARTR_movableMarkers") get _marker;
_markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
_markerVisuals params [
    ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
    ["_markerAlpha", 1, [0]],
    ["_visibleUnassigned", false, [false]],
    ["_markerText", "", [""]]
];

[_marker, _position] remoteExec ["setMarkerPosLocal", _visibility];
[_marker, _rotation] remoteExec ["setMarkerDirLocal", _visibility];
[_marker, _text] remoteExec ["setMarkerTextLocal", _visibility];
