/*
Terminates movable marker system.

Syntax:
    [] call ARTR_fnc_mm_initMarker

Params:
    N/A

Return value:
    true
*/
//set system inactive
missionNamespace setVariable ["ARTR_mm_systemActive", nil, true];

//terminate each player's movable marker FSM
{
    _x setVariable ["ARTR_mm_movingActive", false, true];
} forEach (call BIS_fnc_listPlayers);

//reset marker names for each movable marker
{
    _markerRules = _y;
    _markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
    _markerVisuals params [
        ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
        ["_markerAlpha", 1, [0]],
        ["_visibleUnassigned", false, [false]],
        ["_defaultText", "", [""]]
    ];

    [_x, _defaultText] remoteExec ["setMarkerTextLocal", _visibility];
} forEach (missionNamespace getVariable "ARTR_movableMarkers");

//clear movable marker hashmap
missionNamespace setVariable ["ARTR_movableMarkers", nil, true];

//remove disconnect event handler
removeMissionEventHandler ["PlayerDisconnected", missionNamespace getVariable "ARTR_mm_disconnectEH"];

//remove enableMoving JIP handle
remoteExec ["", "ARTR_mm_JIP_ID"];

true
