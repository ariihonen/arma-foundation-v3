/*
Initializes movable marker.

Syntax:
    [markerName, markerOwnership, markerPlacement, markerVisuals] call ARTR_fnc_mm_initMarker

Params:
    0 - markerName: String - name of existing marker to make movable

    1 - markerOwnership: Array of object/group names as strings
        Owner is first found player that matches condition
            Object: object is local to the machine
            Group: leader of the group is local to the machine

    2 - markerPlacement (Optional): Array [includeAreas, excludeAreas, waterMode, maxGrad, shoreMode]
        0 - includeAreas (Optional, default nil): Array of markers/triggers - allowed areas for marker to be moved to
        1 - excludeAreas (Optional, default []): Array of markers/triggers - areas marker can't be moved to
        2 - waterMode (Optional, default 0): Number - water mode. Could be one of:
            0 - land only
            1 - either
            2 - water only
        3 - checkEmpty (Optional, default true): Boolean - whether to check position can accommodate a unit or not
        4 - vehicleType (Optional, default "B_Soldier_F"): String - Class name of a vehicle to accommodate

    3 - markerVisibility (Optional): Array - definitions for marker visuals
        0 - visibility (Optional, default 0): Number/Object/String/Side/Group/Array
            Same as remoteExec targets - rules for who to make the marker visible for

            Number (See also Machine Network ID)
                0: global
                2: only on server
                Other number: where clientOwner matches the number
                Negative number: inverted effect

            Object: executed where given object is local

            String: identifier (variable name) - executed where the object or group identified by the variable with the provided name is local

            Side: executed on machines where the player is on the specified side

            Group: executed on machines where the player is in the specified group

            Array: combination of the types listed above
        1 - markerAlpha (Optional, default 1): Number [0,1] - alpha to set the marker to
        2 - visibleUnassigned (Optional, default false): Boolean - whether to show marker while unassigned or not

Return value:
    object (marker owner)
*/

params [
    ["_markerName", "", [""]],
    ["_markerOwnership", [], [[]]],
    ["_markerPlacement", [], [[]]],
    ["_markerVisuals", [], [[]]]
];

_markerOwnership = [_markerOwnership, objNull];

_markerPlacement params [
    ["_includeAreas", nil, [[]]],
    ["_excludeAreas", [], [[]]],
    ["_waterMode", 0, [0]],
    ["_checkEmpty", true, [true]],
    ["_vehicleType", "B_Soldier_F", [""]]
];
_markerPlacement = [_includeAreas, _excludeAreas, _waterMode, _checkEmpty, _vehicleType];

_markerVisuals params [
    ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
    ["_markerAlpha", 1, [0]],
    ["_visibleUnassigned", false, [false]]
];
_markerVisuals = [_visibility, _markerAlpha, _visibleUnassigned, markerText _markerName];

//add marker to global movable marker list
_movableMarkers = missionNamespace getVariable "ARTR_movableMarkers";
_movableMarkers set [_markerName, [_markerOwnership, _markerPlacement, _markerVisuals]];
missionNamespace setVariable ["ARTR_movableMarkers", _movableMarkers, true];

//if marker is supposed to be visible with no owner, set visibility to assigned alpha
if (_visibleUnassigned) then { [_markerName, _markerAlpha] remoteExec ["setMarkerAlphaLocal", _visibility]; };

//set marker owner
_markerOwner = [_markerName] call ARTR_fnc_mm_markerOwner;

_markerOwner
