/*
Initializes movable marker system.

Syntax:
    [] call ARTR_fnc_mm_initSystem

Params:
    0 - denyMarker (optional): String - name of marker used to indicate disallowed marker position

Return value:
    true
*/
params ["_denyMarker"];

missionNamespace setVariable ["ARTR_mm_systemActive", true, true];

//create movable markers hashmap
_markerMap = createHashMap;
missionNamespace setVariable ["ARTR_movableMarkers", _markerMap, true];

//create deny marker
if (isNil "_denyMarker") then
{
    _denyMarker = createMarker ["ARTR_mm_mrk_denyMarker", [0,0,0]];
    _denyMarker setMarkerAlpha 0;
    _denyMarker setMarkerColor "ColorRed";
    _denyMarker setMarkerShape "ICON";
    _denyMarker setMarkerType "mil_objective_noShadow";
};
missionNamespace setVariable ["ARTR_mm_denyMarker", _denyMarker, true];

//[] remoteExec ["ARTR_fnc_mm_enableMoving", 0, "ARTR_mm_JIP_ID"];

//create event handlers to handle players disconecting and new ones joining
_disconnectEH = addMissionEventHandler ["HandleDisconnect",
{
	params ["_unit", "_id", "_uid", "_name"];
    _markers = _unit getVariable ["ARTR_mm_ownedMarkers", []];

    [_unit, _markers] spawn {
        params ["_unit", "_markers"];

        waitUntil {isNull _unit || {!alive _unit}};

        { [_x] call ARTR_fnc_mm_markerOwner; } forEach _markers;
    };

    false;
}];
missionNamespace setVariable ["ARTR_mm_disconnectEH", _disconnectEH];
