/*
Sets text on marker to match unit's name

Syntax:
    [marker] remoteExec ["ARTR_fnc_mm_setMarkerOwnerText", unit]

Params:
    0 - marker: String - marker name to append unit name to

Return value:
    None
*/

params ["_marker"];
_markerRules = (missionNamespace getVariable "ARTR_movableMarkers") get _marker;
_markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
_markerVisuals params [
    ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
    ["_markerAlpha", 1, [0]],
    ["_visibleUnassigned", false, [false]],
    ["_defaultText", "", [""]]
];

_text = _defaultText + " (" + profileName + ")";

[_marker, markerPos _marker, markerDir _marker, _text] call ARTR_fnc_mm_changeMarker;
