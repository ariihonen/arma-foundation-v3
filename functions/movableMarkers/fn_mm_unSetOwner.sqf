/*
Un-set marker ownership

Syntax:
    [unit, marker] call ARTR_fnc_mm_unSetOwner

Params:
    0 - unit: Object - unit to remove the marker from
    1 - marker: String - marker name to remove from the unit

Return value:
    boolean
        True: unit had the marker
        False: unit didn't have the marker
*/
params ["_unit", "_marker"];

_markerList = _unit getVariable ["ARTR_mm_ownedMarkers", []];

_markerIndex = _markerList findIf {_x == _marker};

if (_markerIndex > -1) then
{
    _markerList deleteAt _markerIndex;
    _unit setVariable ["ARTR_mm_ownedMarkers", _markerList, true];

    //remove player name from marker text
    _markerRules = (missionNamespace getVariable "ARTR_movableMarkers") get _marker;
    _markerRules params ["_markerOwnership, _markerPlacement, _markerVisuals"];
    _markerVisuals params [
        ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
        ["_markerAlpha", 1, [0]],
        ["_visibleUnassigned", false, [false]],
        ["_defaultText", "", [""]]
    ];

    [_marker, markerPos _marker, markerDir _marker, _defaultText] call ARTR_fnc_mm_changeMarker;

    true
};

false
