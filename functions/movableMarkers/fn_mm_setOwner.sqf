/*
Set marker ownership

Syntax:
    [unit, marker] call ARTR_fnc_mm_setOwner

Params:
    0 - unit: Object - unit to add the marker for
    1 - marker: String - marker name to add for the unit

Return value:
    true
*/
params ["_unit", "_marker"];

_markerMap = missionNamespace getVariable "ARTR_movableMarkers";
_markerRules = _markerMap get _marker;
_markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
_markerVisuals params [
    ["_visibility", 0, [0, objNull, "", west, grpNull, []]],
    ["_markerAlpha", 1, [0]],
    ["_visibleUnassigned", false, [false]],
    ["_defaultText", "", [""]]
];

_markerOwnership params [
    ["_ownerCondition", ["player"], [[]]],
    ["_currentOwner", objNull, [objNull]]
];
_markerOwnership set [1, _unit];

_markerRules = [_markerOwnership, _markerPlacement, _markerVisuals];
_markerMap set [_marker, _markerRules];
missionNamespace setVariable ["ARTR_movableMarkers", _markerMap, true];

if !(isNull _unit) then
{
    _markerList = _unit getVariable ["ARTR_mm_ownedMarkers", []];
    _markerList pushBackUnique _marker;
    _unit setVariable ["ARTR_mm_ownedMarkers", _markerList, true];

    //set marker alpha
    [_markerName, _markerAlpha] remoteExec ["setMarkerAlphaLocal", _visibility];

    //set marker text to include owner name
    [_marker] remoteExec ["ARTR_fnc_mm_setMarkerOwnerText", _unit];
};

true
