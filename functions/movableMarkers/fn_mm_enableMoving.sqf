/*
Initializes marker moving system on player

Syntax:
    [] call ARTR_fnc_mm_enableMoving

Params:
    N/A

Return value:
    None
*/
if !(hasInterface) exitWith {};

[] spawn {
    waitUntil {!isNil "bis_fnc_init"};

    if !(missionNamespace getVariable ["ARTR_mm_systemActive", false]) exitWith {};

    if !(player getVariable ["ARTR_mm_movingActive", false]) then
    {
        player setVariable ["ARTR_mm_movingActive", true, true];

        _id = execFSM "fsm\movableMarkers.fsm";
        _id setFSMVariable ["_fsmId", _id];
        player setVariable ["ARTR_mm_fsmId", _id];

        //reassign all markers on server
        [] remoteExec ["ARTR_fnc_mm_assignAllMarkers", 2];
    };
};
