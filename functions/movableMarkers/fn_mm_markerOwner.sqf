/*
Get marker owner based on marker rules, optionally setting the owner

Syntax:
    [markerName, setOwner] call ARTR_fnc_mm_markerOwner

Params:
    0 - markerName: String or ARRAY - name of existing marker to check owner for, or list of marker names
    1 - setOwner (Optional, default true) - sets whether to set new owner or just find the correct one

Return value:
    object (marker owner)
*/

params [
    ["_markerName",[],["",[]]],
    ["_setOwner",true,[true]]
];

if (_markerName isEqualType []) then
{
    _ownerList = [];
    {
        _owner = [_x, _setOwner] call ARTR_fnc_mm_markerOwner;
        _ownerList pushBack _owner;
    } forEach _markerName;

    _ownerList
} else {
    _markerRules = (missionNamespace getVariable "ARTR_movableMarkers") get _markerName;
    _markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
    _markerOwnership params [
        ["_ownerCondition", ["player"], [[]]],
        ["_currentOwner", objNull, [objNull]]
    ];

    //check through ownerCondition to find appropriate owner
    _newOwner = objNull;
    _ownerIndex = _ownerCondition findIf {
        _return = false;

        if !(isNil _x) then
        {
            _x = call compile _x;

            _unit = _x;
            if (_x isEqualType grpNull) then { _unit = leader _x; };

            if (alive _unit && isPlayer _unit) then { _newOwner = _unit; _return = true; };
        };

        _return
    };

    //set new owner if appropriate
    if (_setOwner) then
    {
        if (_newOwner != _currentOwner) then
        {
            if !(isNull _currentOwner) then { [_currentOwner, _markerName] call ARTR_fnc_mm_unSetOwner; };
            [_newOwner, _markerName] call ARTR_fnc_mm_setOwner;
        };
    };

    _newOwner
};
