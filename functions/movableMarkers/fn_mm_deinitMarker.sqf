/*
Removes marker movement from specific markers.

Syntax:
    [markers] call ARTR_fnc_mm_changeMarker

Params:
    0 - marker: String or Array - name of marker to be disabled, or array of marker names

Return value:
    true
*/
params ["_marker"];

if (_marker isEqualType []) then {
    {
        [_x] call ARTR_fnc_mm_deinitMarker;
    } forEach _marker;

    true
};

_markerMap = missionNamespace getVariable "ARTR_movableMarkers";

//remove marker from current owner
_markerRules = _markerMap get _marker;
_markerRules params ["_markerOwnership", "_markerPlacement", "_markerVisuals"];
_markerOwnership params [
    ["_ownerCondition", ["player"], [[]]],
    ["_currentOwner", objNull, [objNull]]
];
[_currentOwner, _marker] call ARTR_fnc_mm_unSetOwner;

//remove marker from ownership hashmap
_markerMap deleteAt _marker;

true
