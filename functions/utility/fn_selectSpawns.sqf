//selects spawns from a given set (maybe hashing pre-set sets)

//receives: list of hashmap keys, min amount, max amount
    //key can be: any valid hashmap key, object
    //if object, positions returned are translated to global coordinates from object relative coordinates

params ["_spawnMap", "_keyList", "_minSpawns", "_maxSpawns"];

//create spawn hashmap if not already created
/*
if (isNil "ARTR_spawnPoses") then
{
    #include "..\spawnsBuilding.sqf"
    #include "..\spawnsMission.sqf"

    ARTR_spawnPoses = createHashMap;
    ARTR_spawnPoses merge _buildingSpawns;
    ARTR_spawnPoses merge _missionSpawns;
};

if (isNil "ARTR_spawnPoses") exitWith { [] };
*/

#include "..\spawnsBuilding.sqf"
_spawnMap merge [_buildingSpawns, false];

//go through full keylist, adding coordinates to return list based on key type (if type is object, handle position list as relative to object)
_returnList = [];
{
    _poseList = [];
    if (_x isEqualType objNull) then
    {
        _key = typeOf _x;
        _reference = _x;
        _poseListRelative = _spawnMap getOrDefault [_key, [], false];

        //IF NOT EXISTING, BUILDING POSITIONS, NEED TO ALSO INSERT DIR THERE

        {
            _x params ["_position", "_rotation"];
            _posNew = _reference modelToWorldWorld _position;
            _dirNew = (getDir _reference) + _rotation;

            _poseList pushBack [_posNew, _dirNew];

        } forEach _poseListRelative;
    } else {
        _poseList = _spawnMap getOrDefault [_x, [], false];
    };

    _returnList append _poseList;
} forEach _keyList;

_returnList = [_returnList, _minSpawns, _maxSpawns] call ARTR_fnc_chooseRandoms;

_returnList
