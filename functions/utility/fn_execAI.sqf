/*
    [params, functionName, spawn (BOOL), JIP] call ARTR_fnc_execAI

    Executes the passed script/function on whichever machine controls the AI. Run on server.

    Parameters:
        params:        ARRAY - anything (function/command parameters)
        functionName:  STRING - executed function or command name
        call:          BOOL (optional: default false) - true to call command, false to spawn
        JIP:           Optional, default false - same as remoteExec JIP

    Example:
        [[], "ARTR_spawnAI"] call ARTR_fnc_execAI
*/
_parameters = param [0, [], []];
_functionName = param [1, "", [""]];
_call = param [2, false, [true]];
_jip = param [3, false, [true]];

_target = missionNamespace getVariable ["ARTR_HCClient", 2];
if _call then
{
    _parameters remoteExecCall [_functionName, _target, _jip];
} else {
    _parameters remoteExec [_functionName, _target, _jip];
};
