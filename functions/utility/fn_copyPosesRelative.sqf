params ["_reference"];

_listData = [typeOf _reference];
{
    _listData append ([_x, _reference] call ARTR_fnc_poseRelative);
} forEach (get3DENSelected "object");

collect3DENHistory {
    delete3DENEntities (get3DENSelected "object");
};

_br = ("," + toString [13,10]);
_stringList = _listData joinString _br;
copyToClipboard (typeOf _reference) + _stringList;
