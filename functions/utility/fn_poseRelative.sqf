params ["_unit", "_reference"];

_unitPosAGL = ASLToAGL (getPosASL _unit);
_posRelative = _reference worldToModel _unitPosAGL;
_dirRelative = (getDir _unit) - (getDir _reference);

[_posRelative, _dirRelative]
