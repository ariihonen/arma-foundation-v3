//adds state to active mission state list
params ["_state"];

_states = missionNamespace getVariable ["ARTR_missionStates", []];

if (_state isEqualType []) then
{
    { _states pushBack _x; } forEach _state;
} else {
    _states pushBack _state;
};

missionNamespace setVariable ["ARTR_missionStates", _states, true];
