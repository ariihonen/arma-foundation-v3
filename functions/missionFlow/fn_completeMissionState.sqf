//completes mission state (adds to completed state list)
params ["_state"];

_completeStates = missionNamespace getVariable ["ARTR_completeStates", []];
_completeStates pushBackUnique _state;
missionNamespace setVariable ["ARTR_completeStates", _completeStates, true];
