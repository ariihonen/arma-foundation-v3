//checks if mission state is completed but still registers active
params ["_state"];

[_state] call ARTR_fnc_isStateComplete && [_state] call ARTR_fnc_isStateActive
