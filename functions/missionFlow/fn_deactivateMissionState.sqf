//deactivates mission state (removes from active list)
params ["_state"];

_activeStates = missionNamespace getVariable ["ARTR_missionStates", []];
_activeStates deleteAt (_activeStates find _state);
missionNamespace setVariable ["ARTR_missionStates", _activeStates, true];
