/*
	Author: Jiri Wainar - modified by Artturi Riihonen

	Description:
	Play set of ambient animations on given unit AND allows the unit to leave the ambient state and engage enemy or move away.

	Remarks:
	* Function controls BIS_fnc_ambientAnim; check that function [Remarks] section for more info of how to use it.
	* Unit automatically leaves the animation loop if there is an enemy in 300m he knows about.

	Parameter(s):
	0: OBJECT - unit the anim & gear changes are going to be applied to
	1: STRING (optional, default "STAND") - animation set id, describing what the unit's action looks like.
	   > "STAND" 			- standing still, slightly turning to the sides. Needs a rifle!
	   > "STAND_IA"			- default a3 animations for standing, rifle lowered
	   > "SIT_LOW"			- sitting on the ground, with weapon.
	   > "KNEEL"			- kneeling, with weapon.
	   > "LEAN"			- standing while leaning (on wall)
	   > "WATCH"/"WATCH1"/"WATCH2"	- standing and turning around

       Note: can also be list, in which case a random list element is chosen

	2: STRING (optional, default "RANDOM") - equipment level id, describing how heavily is the unit equipped.
	   > "NONE"  		- no goggles, headgear, vest, weapon
	   > "LIGHT"  		- no goggles, headgear, vest
	   > "MEDIUM" 		- no goggles, headgear
	   > "FULL"  		- no goggles
	   > "ASIS" (default)	- no touches to the gear
	   > "RANDOM" 		- gear is randomized according to the animation set

	3: CODE or STRING (optional, default {false}) - condition that if true frees the unit from the animation loop

	4: STRING (optional, default "COMBAT") - behaviour the unit should go to, when freed.

	Returns:
	-

	Example:
	[this,["STAND","WATCH1"],"FULL",{(player distance _this) < 5}] call BIS_fnc_ambientAnimCombat;
*/

//do the immediate operations ----------------------------------------------------------------------
#define ACCEPTABLESTATES ["STAND", "STAND_IA", "SIT_LOW", "KNEEL", "LEAN", "WATCH", "WATCH1", "WATCH2"]
#define ACCEPTABLEGEAR ["NONE", "LIGHT", "MEDIUM", "FULL", "ASIS", "RANDOM"]
#define TRANSANIM "AmovPercMstpSrasWrflDnon"

params [
    ["_unit", objNull, [objNull]],
    ["_animset", "STAND", ["",[]]],
    ["_gear", "ASIS", [""]],
    ["_cond", {false}, [{},""]],
    ["_behaviour", "COMBAT", [""]]
];

if (_animSet isEqualType []) then { _animSet = selectRandom _animSet; };

if !(_animset in ACCEPTABLESTATES) exitWith
{
	format["Definition of animset '%1' dosn't exist. Possible animsets are %2.",_animset,_acceptableStates] call BIS_fnc_error;
};

if !(_gear in ACCEPTABLEGEAR) exitWith
{
	format["Definition of gearset '%1' dosn't exist. Possible gearsets are %2.",_gear,_acceptableGear] call BIS_fnc_error;
};

if (_cond isEqualType "") then { _cond = compile _cond; };

//Save existing state for restore when terminating
_unitPos = unitPos _unit;
_move = _unit checkAIFeature "MOVE";
_path = _unit checkAIFeature "PATH";

//execute the ambient anim
[_unit,_animset,_gear,nil,nil,false] call BIS_fnc_ambientAnim;

//Wait for condition and terminate animation
[_unit,_cond,_behaviour,_unitPos,_move,_path] spawn
{
	private["_unit","_cond","_animHandle","_behaviour","_unitPos","_move","_path"];

    params ["_unit","_cond","_behaviour","_unitPos","_move","_path"];

	//wait for system to initialize on the unit
	waitUntil
	{
		sleep 0.1;

		_animHandle = _unit getVariable ["BIS_EhAnimDone", -1];

		(_animHandle > -1)
	};

	//wait for unlock condition evals to true
	waitUntil
	{
		sleep 0.1;

		(behaviour _unit == "COMBAT") || {(damage _unit > 0) || {(_unit call _cond) || {(_unit call BIS_fnc_enemyDetected)}}}
	};

    //remove the Ai handcuffs
	{_unit enableAI _x} forEach ["ANIM", "AUTOTARGET", "FSM", "TARGET"];
    _unit enableAIFeature ["MOVE", _move];
    _unit enableAIFeature ["PATH", _path];
	_unit setBehaviour _behaviour;
	_unit setUnitPos "UP";
	detach _unit;

    //unlock the unit from its ambient animation
	_unit removeEventHandler ["AnimDone",_animHandle];
	_unit playMoveNow TRANSANIM;

	sleep ((random 3) + 3);

	//return to the default or previously set unit pos
	_unit setUnitPos _unitPos;
};
